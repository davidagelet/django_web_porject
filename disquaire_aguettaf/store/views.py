from django.shortcuts import render
from django.http import HttpResponse
from .models import Album, Artist, Contact, Booking
from django.template import loader

def index(request):
    albums = Album.objects.filter(available=True).order_by("-created_at")[:12]
    formatted_albums = ["<li>{}</li>".format(album.title) for album in albums]
    #message = """<ul>{}</ul> """.format("\n".join(formatted_albums))
    template = loader.get_template('store/index.html')
    #message = "Salut tout le monde !"
    context= {'albums':albums}
    return render(request, 'store/index.html', context)

def listing(request):
    albums = Album.objects.filter(available=True)
    #formatted_albums = ["<li>{}</li>".format(album.title) for album in albums]
    #message = """<ul>{}</ul> """.format("\n".join(formatted_albums))
    context= {'albums':albums}
    #albums = ["<li>{}</li>".format(album['name'])for album in ALBUMS]
    #message = """<ul>{}</ul>""".format("\n".join(albums))
    return render(request, 'store/listing.html', context)

def detail(request, album_id):
    album=Album.objects.get(pk=album_id)
    #id = int(album_id)
    #album = ALBUMS[id]
    #artists=album.artist.all() #pour recuperer tout les artists
    artists_name = " ".join([artist.name for artist in album.artists.all()])
    #artists = " ".join ([artist['name'] for artist in album['artists']])
    #message= "Le nom de l'album est {}. Il a été écrit par {}".format(album.title, artists)
    context= {
        'album_title':album.title,
        'artists_name' : artists_name,
        'album_id' : album.id,
        'thumbnail' : album.picture
    }
    return render(request, 'store/detail.html', context)

def search (request):
    #si on veut recuperer et juste afficher les paraametre cité dans la requete
    #obj=str(request.GET) # tout les parametres passés dans la requete
    #query = request.GET['query']
    #message= f"proprieté GET : {obj} et requete : {query}"
    #return HttpResponse(message)

    query = request.GET.get('query')
    if not query:
        #message = "Aucun artiste n'est demandé"
        albums = Album.objects.all()
    else:
        """albums =[
            album for album in ALBUMS
            if query in " ".join(artist['name'] for artist in album['artists'])
        ]"""
        albums = Album.objects.filter(title__icontains=query)

        if not albums.exists(): #len(albums) == 0:
            albums=Album.objects.filter(artists__name__icontains=query)

        if not albums.exists():
            message ="Misère de misère, nous n'avons trouvé aucun résultat !"
        else:
            albums = ["<li>{}</li>".format(album.title) for album in albums]
            message = """
                Nous avons trouvé les albums correspondant à votre requète ! Les voici :
                <ul>
                        {}
                </ul>
            """.format("</li><li>".join(albums))
        title = "Résultats pour la requete %s"%query
        context = {
        'albums': albums,
        'title': title
        }
    return render(request, 'store/search.html', context)
